var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0],
    i,
    imax,
    maxPoint = 0,
    indexMaxPoint,
    newStudents = ["Николай Фролов", 0, "Олег Боровой", 0];

console.log("Список студентов:");
for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
    console.log("Студент %s набрал %d", studentsAndPoints[i], studentsAndPoints[i + 1]);
    if (studentsAndPoints[i + 1] > maxPoint) {
        maxPoint =  studentsAndPoints[i + 1];
        indexMaxPoint = i + 1;
    }
}
console.log("Студент, набравший максимальный балл:");
console.log("Студент %s имеет максимальный бал %d", studentsAndPoints[indexMaxPoint - 1], maxPoint);
studentsAndPoints = studentsAndPoints.concat(newStudents);
console.log("Студенты, не набравшие баллов:");
if (studentsAndPoints.indexOf("Антон Павлович") === -1) {
    studentsAndPoints.push("Антон Павлович");
    studentsAndPoints.push(10);
} else {
    studentsAndPoints[studentsAndPoints.indexOf("Антон Павлович") + 1] += 10;
}
if (studentsAndPoints.indexOf("Николай Фролов") === -1) {
    studentsAndPoints.push("Николай Фролов");
    studentsAndPoints.push(10);
} else {
    studentsAndPoints[studentsAndPoints.indexOf("Николай Фролов") + 1] += 10;
}
for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
    if (studentsAndPoints[i + 1] === 0) {
        console.log(studentsAndPoints[i]);
        studentsAndPoints.splice(i, 2);
        i -= 2;
    }
}
console.log(studentsAndPoints);